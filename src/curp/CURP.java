/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package curp;



/**
 *
 * @author Angelica Reyes Arellano
 */
public class CURP {
        String nombre;
        String apellidoPaterno;
        String apellidoMaterno;
        String mes;
        String fechaNacimiento;
        String estado;
        String sexo;
        char consonantes;
        String filtraInconvenientes;
        String curp = new String();
         
   /**
     * Metodo que regresa el primer caracter del nombre (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param nombre Nombre(s) del usuario.
     * @return El primer caracter del nombre.
     */
    String nombre(String nombre){
        
        
        return (("" + nombre.charAt(0)).toUpperCase() );
    }
    
    /**
     * Metodo que regresa los primeros dos caracteres del apellido paterno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_p El apellido paterno del usuario.
     * @return Los dos primero caracteres del apellido paterno.
     */
    String apellidoPaterno(String apellido_p){
          apellido_p = apellido_p.replace("á" , "a");
          apellido_p = apellido_p.replace("é" , "e");
          apellido_p = apellido_p.replace("í" , "i");
          apellido_p = apellido_p.replace("ó" , "o");
          apellido_p = apellido_p.replace("ú" , "u");
          apellido_p = apellido_p.replace("à" , "a");
          apellido_p = apellido_p.replace("è" , "e");
          apellido_p = apellido_p.replace("ì" , "i");
          apellido_p = apellido_p.replace("ò" , "o");
          apellido_p = apellido_p.replace("ù" , "u"); 
          apellido_p = apellido_p.replace("ñ" , "n");
        return (("" + apellido_p.substring(0, 2)).toUpperCase() );
        }
    
    
    /**
     * Metodo que regresa el primer caracter del apellido materno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_m El apellido materno del usuario.
     * @return El primer caracter del apellido materno.
     */
    String apellidoMaterno(String apellido_m){
        
            apellido_m = apellido_m.replace("á" , "a");
            apellido_m = apellido_m.replace("é" , "e");
            apellido_m = apellido_m.replace("í" , "i");
            apellido_m = apellido_m.replace("ó" , "o");
            apellido_m = apellido_m.replace("ú" , "u");
            apellido_m = apellido_m.replace("à" , "a");
            apellido_m = apellido_m.replace("è" , "e");
            apellido_m = apellido_m.replace("ì" , "i");
            apellido_m = apellido_m.replace("ò" , "o");
            apellido_m = apellido_m.replace("ù" , "u");
            apellido_m = apellido_m.replace("ñ" , "n");
            return (("" + apellido_m.charAt(0)).toUpperCase());
        
        
    }
    
    /**
     * Metodo que regresa el mes en formato: mm.
     * El metodo debe de aceptar el nombre del mes sin importar si vienen en minusculas o
     * mayusculas o mixto, tambien debe de aceptar si el mes fue dado como un entero.
     * Ejemplos de posibles entradas:
     * - Enero
     * - enero
     * - EnErO
     * - 01
     * - 1
     * Para cualquiera de las entradas anteriores la cadena que debe regresar es: 01
     * @param mes El mes dado.
     * @return El mes en formato: mm.
     */
    String getMes(String mes){
        this.mes = mes.toLowerCase();
        switch (mes){
                        
                        case "enero":
			estado = "01";
			break;
                        case "febrero":
			estado = "02";
			break;
                        case "marzo":
			estado = "03";
			break;
                        case "abril":
			estado = "04";
			break;
                        case "mayo":
			estado = "05";
			break;
                        case "junio":
			estado = "06";
			break;
                        case "julio":
			estado = "07";
			break;
                        case "agosto":
			estado = "08";
			break;
                        case "septiembre":
			estado = "09";
			break;
                        case "octubre":
			estado = "10";
			break;
                        case "noviembre":
			estado = "11";
			break;
                        case "diciembre":
			estado = "12";
			break;
                         }
        return ( this.mes );
        
    }
    
    /**
     * Metodo que regresa la fecha de nacimiento en el formato correcto: aammdd
     * La fecha viene en el siguiente formato: dd/mm/aaaa
     * Hint: Usen el metodo split para obtener los datos.
     * @param fecha La fecha de nacimiento del usuario.
     * @return La fecha en formato: aammdd.
     */
    String fechaNacimiento(String fecha){
        
        String [ ] fechaNacimiento = fecha.split("/") ;
        
        return ( fechaNacimiento[2].substring(2).trim() + getMes(fechaNacimiento[1]) + fechaNacimiento[0].trim() );
    }
    
    /**
     * Metodo que regresa el primer caracter del sexo dado, en el formato correcto.
     * @param sexo El sexo del usuario.
     * @return El primer caracter del sexo.
     */
    String sexo(String sexo){
       
        return (("" + sexo.charAt(0)).toUpperCase());
    }
    
    /**
     * Metodo que regresa la abreviatura del estado, en el formato correcto.
     * El metodo debe de aceptar entradas en minusculas, mayusculas o mixta,
     * tambien debe de aceptar entradas con acentos y sin acentos.
     * @param estado El estado de nacimiento del usuario.
     * @return La abreviatura del usuario.
     */
    String estado(String estado){
          estado = estado.toLowerCase();
          estado = estado.trim(); // elimina el primer y ultimo espacio
          estado = estado.replace(" ", ""); //elimina los espacios intermedios
          //eliminamos los acentos
          estado = estado.replace("á" , "a"); 
          estado = estado.replace("é" , "e");
          estado = estado.replace("í" , "i");
          estado = estado.replace("ó" , "o");
          estado = estado.replace("ú" , "u");
          //Hacemos los casos de los 32 estados
            switch (estado){
                        
                        case "aguascalientes":
			estado = "AS";
			break;
			case "bajacalifornia":
			estado = "BC";
			break;
			case "bajacaliforniasur":
			estado = "BS";
			break;
			case "campeche":
			estado = "CC";
			break;
			case "chiapas":
			estado = "CS";
			break;
			case "chihuahua":
			estado = "CH";
			break;
			case "coahuila":
			estado = "CL";
			break;
			case "colima":
			estado = "CM";
			break;
			case "ciudaddemexico":
			estado = "DF";
			break;
			case "durango":
			estado = "DG";
			break;
			case "guanajuato":
			estado = "GT";
			break;
			case "guerrero":
			estado = "GR";
			break;
			case "hidalgo":
			estado = "HG";
			break;
			case "jalisco":
			estado = "JC";
			break;
			case "mexico":
			estado = "MC";
			break;
			case "michoacan":
			estado = "MN";
			break;
			case "morelos":
			estado = "MS";
			break;
			case "nayarit":
			estado = "NT";
			break;
			case "nuevoleon":
			estado = "NL";
			break;
			case "oaxaca":
			estado = "OC";
			break;
			case "puebla":
			estado = "PL";
			break;
			case "queretaro":
			estado = "QT";
			break;
			case "quintanaroo":
			estado = "QR";
			break;
			case "sanluispotosi":
			estado = "SP";
			break;
			case "sinaloa":
			estado = "SL";
			break;
			case "sonora":
			estado = "SR";
			break;
			case "tabasco":
			estado = "TC";
			break;
			case "tamaulipas":
			estado = "TS";
			break;
			case "tlaxcala":
			estado = "TL";
			break;
			case "veracruz":
			estado = "VZ";
			break;
                        case "zacatecas":
			estado = "ZS";
			break;
			case "yucatan":
			estado = "YN";
			break;
			case "nacidoenelextranjero":
			estado = "NE";
			break;
        
    }
            return (estado.toUpperCase());
            }
    
    /**
     * Metodo que regresa la segunda consonante de una cadena.
     * Hint: remplacen las vocales con acentos por vocales sin acentos y
     * pasen toda la cadena a minusculas o mayusculas para que tengan menos casos que revisar.
     * @param cadena La cadena dada.
     * @return La segunda consonante de una cadena.
     */
   
    char getConsonantes(String cadena){
        cadena = cadena.toUpperCase();
        cadena = cadena.replace('Ñ', 'N');
        cadena = cadena.replace('Á', 'A');
        cadena = cadena.replace('É', 'E');
        cadena = cadena.replace('Í', 'I');
        cadena = cadena.replace('Ó', 'O');
        cadena = cadena.replace('Ú', 'U');
        cadena = cadena.replace(" ", "");
        cadena = cadena.replace('À', 'A');
        cadena = cadena.replace('È', 'E');
        cadena = cadena.replace('Ì', 'I');
        cadena = cadena.replace('Ò', 'O');
        cadena = cadena.replace('Ù', 'U');
        
        char consonate = ' ';
        for(int i = 1; i < cadena.length(); i++){
            if(cadena.charAt(i) != 'A' && cadena.charAt(i) != 'E' && 
                cadena.charAt(i) != 'I' && cadena.charAt(i) != 'O' && 
                    cadena.charAt(i) != 'U'){
                consonate = cadena.charAt(i);
                break;
            }
        }
         return (consonate );
       }
    }
